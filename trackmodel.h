#ifndef TRACKMODEL_H
#define TRACKMODEL_H

#include <QAbstractListModel>
#include <QVector>
#include <QGeoCoordinate>
#include <QColor>

class TrackModel : public QAbstractListModel
{
    Q_OBJECT

    struct Mark
    {
        QGeoCoordinate position;
        QColor color;
    };

public:
    enum {
        MarkPositionRole = Qt::UserRole + 1,
        MarkColorRole,
        PathLineRole
    };

    explicit TrackModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE
    void addMark(const QGeoCoordinate& position, const QColor& color);

    Q_INVOKABLE
    void insertAfter(const QColor& color,int index);

    Q_INVOKABLE
    void removeMark(int index);

private:
    QVector<Mark> m_marks;
    int lastMarkIndex = 0;
    int markPlacementIndex = 0;
};

#endif // TRACKMODEL_H
