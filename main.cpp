#include <QGuiApplication>
#include <QQmlContext>
#include <QQmlApplicationEngine>

#include "trackmodel.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    TrackModel trackModel;

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("trackModel", &trackModel);

    engine.load(QStringLiteral("qrc:/main.qml"));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
