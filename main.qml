import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

ApplicationWindow {
    property color selectedMarkColor: 'red'


    width: 640; height: 480
    visible: true

    RowLayout {
        anchors.fill: parent
        spacing: 0

        MarkedMap {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumWidth: 320
            Layout.minimumHeight: 240
            Layout.preferredWidth: 640
        }

        MarkList {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumWidth: 80
            Layout.minimumHeight: 240
            Layout.preferredWidth: 100
            Layout.maximumWidth: 120
        }
    }
}
