import QtQuick 2.0
import QtQuick.Controls 2.12

ListView {
    readonly property int itemHeight: 40

    id: list

    model: listModel
    delegate: listDelegate

    highlight: highlightBar
    highlightFollowsCurrentItem: false

    focus: true

    Menu {
        id: contextMenu
        MenuItem {
            text: qsTr('Delete')
        }
    }

    ListModel {
        id: listModel
        ListElement {
            name: 'nn'
        }
        ListElement {
            name: 'dd'
        }
    }

    Component {
        id: listDelegate
        Rectangle {
            width: list.width
            height: itemHeight
            color: mouseArea.containsMouse ? 'gray' : 'transparent'

            Text {
                id: itemText
                text: name
            }

            MouseArea {
                id: mouseArea
                anchors.fill: parent
                hoverEnabled: true
                acceptedButtons: Qt.LeftButton | Qt.RightButton
                onClicked: {
                    list.currentIndex = index
                    if (mouse.button == Qt.RightButton)
                        contextMenu.popup()
                }
            }
        }
    }

    Component {
        id: highlightBar
        Rectangle {
            width: list.width
            height: itemHeight
            y: list.currentItem.y
            color: 'blue'
        }
    }
}
