#include "trackmodel.h"

#include <QDebug>

TrackModel::TrackModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int TrackModel::rowCount(const QModelIndex &parent) const
{
    return m_marks.size();
}

QVariant TrackModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= m_marks.size())
        return QVariant();

    switch (role)
    {
        case MarkPositionRole:
            return QVariant::fromValue(m_marks.at(index.row()).position);

        case MarkColorRole:
            return QVariant::fromValue(m_marks.at(index.row()).color);

        case PathLineRole:
        {
            if (m_marks.size() < 2)
                return QVariant();

            const QVector<QGeoCoordinate> pathLine{m_marks.at(index.row() - 1).position,
                                                   m_marks.at(index.row()    ).position};
            return QVariant::fromValue(pathLine);
        }
    }

    return QVariant();
}

bool TrackModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid())
        return false;

    if (index.row() >= m_marks.size())
        return false;

    switch (role)
    {
        case MarkPositionRole:
        {
            const auto coordinate = qvariant_cast<QGeoCoordinate>(value);
            if (!coordinate.isValid())
            {
                if (m_marks.size() > 1)
                m_marks[index.row()].position = m_marks[lastMarkIndex + 1].position;
            }
            else
                m_marks[index.row()].position = coordinate;

            emit dataChanged(index, index, {MarkPositionRole});

            if (index.row() < m_marks.size() - 1)
                emit dataChanged(index, this->index(index.row() + 1), {PathLineRole});
            else
                emit dataChanged(index, index, {PathLineRole});
            return true;
        }
        case MarkColorRole:
        {
            const auto color = qvariant_cast<QColor>(value);
            m_marks[index.row()].color = color;

            emit dataChanged(index, index, {MarkColorRole});
            return true;
        }
    }

    return false;
}

QHash<int, QByteArray> TrackModel::roleNames() const
{
    static const QHash<int, QByteArray> roles
    {
        {MarkPositionRole, "markPosition"},
        {MarkColorRole, "markColor"},
        {PathLineRole, "pathLine"}
    };
    return roles;
}

void TrackModel::addMark(const QGeoCoordinate& position, const QColor& color)
{
    beginInsertRows(QModelIndex(), m_marks.size(), m_marks.size());
    m_marks.append({position.isValid() ? position : m_marks.last().position, color});
    endInsertRows();
    lastMarkIndex = m_marks.size();
}

void TrackModel::insertAfter(const QColor& color, int index)
{
    const auto newPosition = QGeoCoordinate(m_marks[index].position.latitude() - 0.001,
                                            m_marks[index].position.longitude());
    beginInsertRows(QModelIndex(), index + 1, index + 1);
    m_marks.insert(index + 1, {newPosition, color});
    endInsertRows();
}

void TrackModel::removeMark(int index)
{
    if (index < 0 || index >= m_marks.size())
        return;

    beginRemoveRows(QModelIndex(), index, index);
    m_marks.remove(index);
    endRemoveRows();
}
