import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Dialogs 1.2
import QtPositioning 5.12
import QtLocation 5.12

Map {
    id: map
    plugin: Plugin {name: 'mapboxgl'}
    center: QtPositioning.coordinate(50.45, 30.52)
    zoomLevel: 14

    MouseArea {
        id: mapMouseArea
        anchors.fill: parent
        hoverEnabled: true

        onClicked: {
            var coordinate = map.toCoordinate(Qt.point(mouseX, mouseY))
            trackModel.addMark(coordinate, selectedMarkColor)
        }
    }

    MapQuickItem {
        coordinate: QtPositioning.coordinate(50.45, 30.52)
        sourceItem: Image {
            width: 50
            height: 50
            source: "images/drone.png"
        }
    }

    MapItemView {
        model: trackModel
        delegate: trackDelegate
    }

    Component {
        id: trackDelegate
        MapQuickItem {
            id: mapQuickItem
            coordinate: markPosition
            sourceItem: ColumnLayout {
                Rectangle {
                    id: rect
                    width: 10; height: 10
                    color: markColor

                    MouseArea {
                        anchors.fill: parent
                        drag.target: mapQuickItem
                        drag.axis: Drag.XAndYAxis
                        acceptedButtons: Qt.LeftButton | Qt.RightButton

                        onClicked: {
                            if (mouse.button == Qt.RightButton)
                                markContextMenu.popup()
                        }
                        onDoubleClicked: {
                            if (mouse.button == Qt.LeftButton)
                                colorDialog.visible = true
                        }
                    }
                }
                Text {
                    text: qsTr("Mark #%1").arg(index.toString())
                }
            }

            onXChanged: markPosition = coordinate
            onYChanged: markPosition = coordinate

            anchorPoint.x: rect.x + rect.width / 2
            anchorPoint.y: rect.y + rect.height / 2

            ColorDialog {
                id: colorDialog
                title: 'Please choose a color'
                onAccepted: {
                    markColor = colorDialog.color
                    selectedMarkColor = colorDialog.color
                }
            }

            Menu {
                id: markContextMenu
                MenuItem {
                    text: qsTr('Color')
                    onTriggered: colorDialog.visible = true
                }
                MenuItem {
                    text: qsTr('Сделать не валидной')
                    onTriggered: markPosition = QtPositioning.coordinate(91, 181)
                }
                MenuItem {
                    text: qsTr('Добавить до')
                    onTriggered: trackModel.insertAfter(selectedMarkColor, index > 0 ? index - 1 : index)
                }
                MenuItem {
                    text: qsTr('Добавить после')
                    onTriggered: trackModel.insertAfter(selectedMarkColor, index)
                }

                MenuItem {
                    text: qsTr('Delete')
                    onTriggered: trackModel.removeMark(index)
                }
            }
        }
    }

    MapItemView {
        model: trackModel
        delegate: MapPolyline {
            id: polyline
            line.width: 2
            line.color: 'red'
            path: index > 0 ? pathLine : []
        }
    }

    MapItemView {
        model: cursorModel

        delegate: MapPolyline {
            line.width: 2
            line.color: 'red'
            path: cursorPath
        }
    }
}
